; LC-3 assembly
; brain fuck compiler
; takes a brain fuck program on STDIN
; compiles it to LC3
; uses a stack to turn [] into direct jumps
; jumps to the generated code after compilation

.ORIG x3000

main
	; R6: int *out // codegen destination address
	; R5: int *stk // top address of stack of [ codegen offsets
	; R4: int *ptr // address of code block
	; R3: int  off // offset into code_meta
	; R2: int  len // length of code block
	; R1: int  y   // scratch register
	; R0: int  x   // scratch register

	LEA R5 stack                ; stk = stack
	;LEA R6 code_start           ; out = code
	LD R6 code_start_p
	BRnzp codegen_loop
	    code_start_p
	        .FILL code_start

	codegen_loop
	GETC                        ; x = getch()
	OUT
	LEA R1 lut                  ; {y = lut}
	ADD R1 R1 R0                ; {y = lut+x}
	LDR R3 R1 #0                ; {off = *y} off = lut[x]
	BRz codegen_loop            ; if off == 0 goto codegen_loop
	    ADD R3 R3 #-1
	    ADD R3 R3 R3            ; off = (off-1)*2
	    ; off: op
		; 0: gt
		; 2: lt
		; 4: plus
		; 6: min
		; 8: dot
		; 10: comma
		; 12: open bracket
		; 14: close bracket
		; 16: halt
		LEA R0 code_meta
		ADD R0 R0 R3
		LDR R4 R0 #0            ; ptr = code_meta[off]
		LDR R2 R0 #1            ; len = code_meta[off+1]
		codegen_L1
			LDR R0 R4 #0        ; x = *ptr
			STR R0 R6 #0        ; *out = x
			ADD R6 R6 #1        ; out++
			ADD R4 R4 #1        ; ptr++
			ADD R2 R2 #-1       ; len--
		BRnp codegen_L1         ; if len goto codegen_L1
		ADD R3 R3 #-12
		BRnp codegen_L2         ; if off != 12 goto codegen_L2
		    ; generated a [
		    ; push(out), note out is location of [ + len of [ instead of location of [
			STR R6 R5 #0        ; *stk = out
			ADD R5 R5 #1        ; skt++
		codegen_L2
		ADD R3 R3 #-2           ; off-=2
		BRnp codegen_l3         ; if off != 14 goto codegen_L3
		    ; generated a ]
			; pop(out)
			AND R1 R0 R0
			ADD R5 R5 #-1       ; stk--
			
            ;LEA R0 msg1
            ;PUTS
			LDR R1 R5 #0        ; x = *stk
			
			; x now contains the address right after the [
			; and current codegen address is right after the ]
			
			; patch up [
			; set the jmp target at x-3 to our current codegen address (out)
			;LEA R0 msg2
            ;PUTS
			STR R6 R1 #-3       ; *(x-3) = out
			
			;LEA R0 msg3
            ;PUTS
			; patch up ]
			; set the jmp target at out-3 to x
			STR R1 R6 #-3       ; *(out-3) = x
		codegen_L3
		ADD R3 R3 #-2
		BRnp codegen_L4         ; if off != 16 goto codegen_L4
		    ; generated a HALT
			AND R0 R0 #0        ; R0 = 0
			LD R1 tape_start_p  ; R1 = tape_start
			BRnzp code_start    ; goto code_start
			tape_start_p
				.FILL tape_start
		codegen_L4
	BRnzp codegen_loop          ; goto codegen_loop

; FOR DEBUGGING
unreachable
    LEA R0 msg
    PUTS
	HALT
msg
    .STRINGZ "ERROR"

code_meta; array of codeblock{addr, len}
	.FILL code_gt
	.FILL 3
	.FILL code_lt
	.FILL 3
	.FILL code_plus
	.FILL 1
	.FILL code_min
	.FILL 1
	.FILL code_dot
	.FILL 1
	.FILL code_comma
	.FILL 1
	.FILL code_open
	.FILL 6; 5 of which are instructions
	.FILL code_close
	.FILL 6; 5 of which are instructions
	.FILL code_halt
	.FILL 1

BRnzp unreachable

; R0: int  cell // cached value of cell at tape head
; R1: int *tape // tape head
; R7: int  z    // scratch register

code_gt; >               // tape++
	STR R0 R1 #0
	ADD R1 R1 #1
	LDR R0 R1 #0

BRnzp unreachable

code_lt; <               // tape--
	STR R0 R1 #0
	ADD R1 R1 #-1
	LDR R0 R1 #0

BRnzp unreachable

code_plus; +             // (*tape)++
	ADD R0 R0 #1

BRnzp unreachable

code_min; -              // (*tape)--
	ADD R0 R0 #-1

BRnzp unreachable

code_dot; .              // printf("%d",*tape)
	OUT

BRnzp unreachable

code_comma; ,            // scanf("%d",tape)
	GETC

BRnzp unreachable

code_open; [             // if *tape == 0 goto `corresponding ]`
	ADD R0 R0 #0         ; set CC flags
	BRnp skip_open       ; if cell != 0 goto skip
	BRz  skip_open_data
	open_data
		.FILL #0         ; address of code of corresponding ]
	skip_open_data
		LD R7 open_data
		JMP R7           ; goto `corresponding ]`
	skip_open            ; skip:

BRnzp unreachable

code_close; ]            // if *tape != 0 goto `prev [`
	ADD R0 R0 #0         ; set CC flags
	BRz skip_close       ; if cell == 0 goto skip
	BRnp skip_close_data
	close_data
		.FILL #0
	skip_close_data
		LD R7 close_data
		JMP R7           ; goto `prev [`
	skip_close           ; skip:

BRnzp unreachable

code_halt; ~
	HALT

BRnzp unreachable

; max supported [] nesting level is 32, more is undefined behaviour (no overflow checks)
; unmatched [] is also undefined behavior (no underflow checks)
stack
	.BLKW #32

BRnzp unreachable

;msg1
;    .STRINGZ "A"
;msg2
;    .STRINGZ "B"
;msg3
;    .STRINGZ "C"

; 128 words
; '>':1, '<':2, '+':3, '-':4, '.':5, ',':6, '[':7, ']':8, '~':9, default:0
lut
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #3; offset 43: +
	.FILL #6; offset 44: ,
	.FILL #4; offset 45: -
	.FILL #5; offset 46: .
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #2; offset 60: <
	.FILL #0
	.FILL #1; offset 62: >
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #7; offset 91: [
	.FILL #0
	.FILL #8; offset 93: ]
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #0
	.FILL #9; offset 126: ~
	.FILL #0

BRnzp unreachable

code_start
	.BLKW #5000

tape_start
	.BLKW #5000

.END